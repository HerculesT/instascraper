# Instagram Scraper

This Python script, called "Instagram Scraper", allows you to scrape follower and following lists from an Instagram profile and compare them to detect differences. 

**NOTE**: The information you desire will be found in a new file named [Differences].

Know who was recently followed or unfollowed ;)

## How It Works

Execute the script once, create the first set of files and keep them.
Later down the road whenever you wanna check the differences simply, run the script again and keep a look at the files.

1. **Initialization**: The script reads the Instagram username, password, and profile name from a `settings.json` file. It then initializes the Instagram loader and logs in with the provided credentials.

2. **Scraping Followers and Following**: Using the Instaloader library, the script scrapes the list of followers and following from the specified Instagram profile.

3. **Comparison**: The script compares the scraped follower and following lists with the previously stored ones to detect any differences.

4. **Writing to Files**: The script writes the scraped follower and following lists to separate text files. If differences are found, it writes them to a "difference.txt" file.

## How to Use

1. **Install Dependencies**: Make sure you have Python installed on your system along with the Instaloader library. You can install Instaloader via pip: 
`pip install instaloader`


2. **Create a Settings File**: Create a `settings.json` file in the same directory as the script with the following structure:

```json
{
    "username": "your_instagram_username",
    "password": "your_instagram_password",
    "profile": "target_profile_name"
}
```

3. **Run the Script**: Execute the script, and it will scrape the follower and following lists from the specified Instagram profile, compare them, and write the results to text files.
   
`python scraper.py`


**Note**

- Ensure that you have proper permissions to scrape data from the target Instagram profile.
- Be mindful of Instagram's rate limits and terms of service to avoid any issues.
- Keep your `settings.json` file secure and do not share it publicly, especially if it contains sensitive information like passwords.

**Dependencies**

- Python 3.x
- Instaloader

**Author**

This script was created by [Your Name]. Feel free to contact me at [Your Email] for any inquiries or suggestions.

Replace [Your Name] and [Your Email] with your actual name and email address.