import json
import os
import instaloader
from tqdm import tqdm


class Scraper:
    followers_list = []
    following_list = []

    def __init__(self):
        self.profile = None
        try:
            with open("settings.json", "r") as settings_file:
                settings = json.load(settings_file)
                self.username = settings["username"]
                self.password = settings["password"]
                self.profile_name = settings["profile"]

            self.create_session()
            self.scrape_following()
            self.scrape_followers()
            self.compare_files()
        except FileNotFoundError:
            print("Error: settings.json file not found.")
        except json.JSONDecodeError:
            print("Error: Invalid JSON format in settings.json.")
        except Exception as e:
            print("An error occurred:", e)

    def create_session(self):
        try:
            print("Logging in please hold.")
            loader = instaloader.Instaloader()
            loader.login(self.username, self.password)  # Login or load session
            self.profile = instaloader.Profile.from_username(loader.context, self.profile_name)
            print("Log in successful!!")
        except instaloader.exceptions.InstaloaderException as e:
            print("Error creating Instaloader session:", e)

    def scrape_followers(self):
        try:
            followers = self.profile.get_followers()
            for follower in tqdm(followers, desc="Scraping followers", unit=" follower"):
                self.followers_list.append(follower.username)
            self.write_file(self.followers_list, f"{self.profile_name}_followers")
        except AttributeError:
            print("Error: Unable to scrape followers. Make sure the profile name is correct.")

    def scrape_following(self):
        try:
            following = self.profile.get_followees()
            for following in tqdm(following, desc="Scraping following", unit=" following"):
                self.following_list.append(following.username)
            self.write_file(self.following_list, f"{self.profile_name}_following")
        except AttributeError:
            print("Error: Unable to scrape following. Make sure the profile name is correct.")

    def compare_files(self):
        difference = []
        try:
            old_followers = set(self.read_file(f"{self.profile_name}_followers.txt"))
            new_followers = set(self.followers_list)
            followers_difference = new_followers - old_followers
            if followers_difference:
                print("New followers:")
                for follower in followers_difference:
                    difference.append(f"+ {follower}")
                    print(f"+ {follower}")

            removed_followers = old_followers - new_followers
            if removed_followers:
                print("Removed followers:")
                for follower in removed_followers:
                    difference.append(f"- {follower}")
                    print(f"- {follower}")

            old_following = set(self.read_file(f"{self.profile_name}_following.txt"))
            new_following = set(self.following_list)
            following_difference = new_following - old_following
            if following_difference:
                print("New following:")
                for following in following_difference:
                    difference.append(f"+ {following}")
                    print(f"+ {following}")

            removed_following = old_following - new_following
            if removed_following:
                print("Removed following:")
                for following in removed_following:
                    difference.append(f"- {following}")
                    print(f"- {following}")
            if difference:
                self.write_file(difference, "differences")
        except FileNotFoundError:
            print("Error: File not found.")
        except Exception as e:
            print("An error occurred:", e)

    def read_file(self, filename):
        with open(filename, "r") as file:
            return [line.strip() for line in file]

    def write_file(self, data, method_name):
        base_filename = f"{method_name}.txt"
        new_filename = f"{method_name}_new.txt"

        # If the base filename already exists, write to _new.txt
        if os.path.exists(base_filename):
            filename = new_filename
        else:
            filename = base_filename

        with open(filename, "w") as file:
            if isinstance(data, set):  # For differences set
                for item in data:
                    file.write(item + "\n")
            else:  # For followers and following lists
                for item_value in data:
                    file.write(item_value + "\n")


if __name__ == '__main__':
    Scraper()
